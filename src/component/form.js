import React, { useState } from "react";
import axios from "axios";
import "../component/css/form.module.css";

export default function Form() {
  const [user, setUser] = useState({
    userAccount: "",
    password: "",
  });
  // const [authenticated, setAuthenticated] = useState(
  //   localStorage.getItem(localStorage.getItem("authenticated") || false)
  // );

  const handleLogin = (e) => {
    e.preventDefault();
    axios
      .post("http://localhost:8080/user/login", user)
      .then((response) => {
        console.log(response.data);
        // setAuthenticated(true);
        // localStorage.setItem("authenticated", true);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const handleRegister = (event) => {
    event.preventDefault();
    axios
      .post("http://localhost:8080/user/register", user)
      .then((response) => {
        console.log(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const handleChange = (event) => {
    const { name, value } = event.target;
    setUser((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  return (
    <div>
      <div class="background d-flex justify-content-center">
        <div>
          <form onSubmit={handleLogin}>
            <h3>Login Here</h3>

            <label htmlFor="userAccount">Username</label>
            <input
              type="text"
              name="userAccount"
              onChange={handleChange}
              placeholder="Email or Phone"
            />

            <label htmlFor="password">Password</label>
            <input
              type="password"
              name="password"
              onChange={handleChange}
              placeholder="Password"
            />

            <button>Log In</button>
          </form>
        </div>
        <div className="d-none">
          <form onSubmit={handleRegister}>
            <h3>register</h3>

            <label htmlFor="username">Username</label>
            <input
              type="text"
              name="userAccount"
              onChange={handleChange}
              placeholder="Email or Phone"
            />

            <label htmlFor="password">Password</label>
            <input
              type="password"
              name="password"
              onChange={handleChange}
              placeholder="Password"
            />

            <button type="submit">Regeister</button>
          </form>
        </div>
      </div>
    </div>
  );
}
