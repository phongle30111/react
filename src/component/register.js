import React, { useState } from "react";
import axios from "axios";
import "../component/css/form.module.css";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";

export default function Register() {
  const [user, setUser] = useState({
    userAccount: "",
    password: "",
  });
  const navigate = useNavigate();

  const handleRegister = (event) => {
    event.preventDefault();
    axios
      .post("http://localhost:8080/user/register", user)
      .then(() => {
        navigate("/login");
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const handleChange = (event) => {
    const { name, value } = event.target;
    setUser((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  return (
    <div>
      <div class="background d-flex justify-content-center mb-4">
        <div>
          <form onSubmit={handleRegister}>
            <h3>Register</h3>

            <label htmlFor="username">Username</label>
            <input
              type="text"
              name="userAccount"
              onChange={handleChange}
              placeholder="Email or Phone"
            />

            <label htmlFor="password">Password</label>
            <input
              type="password"
              name="password"
              onChange={handleChange}
              placeholder="Password"
            />
            <label htmlFor="password">Re-Password</label>
            <input
              type="password"
              onChange={handleChange}
              placeholder="Password"
            />
            <Link to="/login">
              <h6 class="mt-4">Login?</h6>
            </Link>
            <div className="mt-4 d-flex justify-content-center">
              <button type="submit">Regeister</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
