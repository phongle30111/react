import React, { useState } from "react";
import Header from "./header";
import Nav from "./nav";
import MedicineList from "./medicineList";
import MedicineDetail from "./medicineDetail";
import Login from "./login";
import Register from "./register";
import AddMedicine from "./addMedicine"
import Cart from "./cart";
import Footer from "./footer";
import Swal from "sweetalert2";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

export default function Home() {
  const [cartItems, setCartItems] = useState([]);
  const [isLoggedIn, setIsLoggedIn] = useState(!!localStorage.getItem("user"));

  const handleAddToCart = (medicine) => {
    Swal.fire({
      icon: "success",
      title: "Thêm thành công!",
      showConfirmButton: true,
    });

    const existingCartItem = cartItems.find((item) => item.id === medicine.id);

    if (existingCartItem) {
      setCartItems((prevState) =>
        prevState.map((item) =>
          item.id === medicine.id
            ? { ...item, quantity: item.quantity + 1 }
            : item
        )
      );
    } else {
      setCartItems((prevState) => [...prevState, { ...medicine, quantity: 1 }]);
    }
  };

  const handleDelete = (medicineId) => {
    Swal.fire({
      title: "Bạn có muốn xóa?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Có",
      cancelButtonText: "Không",
    }).then((result) => {
      if (result.isConfirmed) {
        setCartItems((prevState) =>
          prevState.filter((item) => item.id !== medicineId)
        );
      }
    });
  };

  const handleUpdateQuantity = (medicineId, newQuantity) => {
    if (newQuantity < 1) {
      handleDelete(medicineId);
    } else {
      setCartItems((prevState) =>
        prevState.map((item) =>
          item.id === medicineId ? { ...item, quantity: newQuantity } : item
        )
      );
    }
  };

  return (
    <Router>
      <div className="App container-fluid p-0">
        <Header />
        <Nav
          cartItems={cartItems}
          isLoggedIn={isLoggedIn}
          setIsLoggedIn={setIsLoggedIn}
        />
        <Routes>
          <Route
            index
            element={<MedicineList handleAddToCart={handleAddToCart} />}
          ></Route>
          <Route
            path="/medicine/detail/:id"
            element={<MedicineDetail handleAddToCart={handleAddToCart} />}
          ></Route>
          <Route
            path="/login"
            element={<Login setIsLoggedIn={setIsLoggedIn} />}
          ></Route>
          <Route path="/register" element={<Register />}></Route>
          <Route path="/add-medicine" element={<AddMedicine />}></Route>
          <Route
            path="/cart"
            element={
              <Cart
                cartItems={cartItems}
                handleUpdateQuantity={handleUpdateQuantity}
                handleDelete={handleDelete}
              />
            }
          ></Route>
        </Routes>
        <Footer />
      </div>
    </Router>
  );
}
