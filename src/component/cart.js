import React from "react";
import "../component/css/cart.css";
export default function Cart(props) {
  const { cartItems } = props;
  let totalAmount = 0;

  for (const item of cartItems) {
    totalAmount += item.price * item.quantity;
  }

  return (
    <div className="container mb-5">
      <h2 className="border-bottom pb-5 d-flex justify-content-center">
        Giỏ hàng
      </h2>
      {cartItems.length < 1 && (
        <h1 className="text-danger">Không có sản phẩm nào</h1>
      )}
      {cartItems.length > 0 && (
        <div className="row border-bottom m-0 pb-2">
          <div className="col-2 d-flex justify-content-center">Hình ảnh</div>
          <div className="col-2 d-flex justify-content-center">Tên thuốc</div>
          <div className="col-2 d-flex justify-content-center">Giá</div>
          <div className="col-2 d-flex justify-content-center">Thành tiền</div>
          <div className="col-4 d-flex justify-content-center">Số lượng</div>
        </div>
      )}
      {cartItems.map((item) => (
        <div key={item.id} className="row border-bottom m-0">
          <img src={item.img} className="col-2" alt="Product" />
          <span className="col-2 d-flex align-items-center">
            {item.nameMedicine}
          </span>
          <span className="col-2 d-flex justify-content-center align-items-center">
            {item.price.toLocaleString()}đ/{item.unit}
          </span>
          <span className="col-2 d-flex justify-content-center align-items-center">
            {(item.price * item.quantity).toLocaleString()}đ
          </span>
          <div className="d-flex justify-content-center align-items-center">
            <button
              className="col-1"
              onClick={() =>
                props.handleUpdateQuantity(item.id, item.quantity - 1)
              }
            >
              -
            </button>
            <input
              type="text"
              className="form-control"
              readOnly
              value={item.quantity}
            />
            <button
              className="col-1"
              onClick={() =>
                props.handleUpdateQuantity(item.id, item.quantity + 1)
              }
            >
              +
            </button>
            <button
              className="col-2"
              onClick={() => props.handleDelete(item.id)}
            >
              Xóa
            </button>
          </div>
        </div>
      ))}
      {cartItems.length > 0 && (
        <div className="mt-3 d-flex justify-content-end">
          <span className="font-weight-bold mr-3">Tổng tiền:</span>
          <h4>{totalAmount.toLocaleString()}đ</h4>
        </div>
      )}
    </div>
  );
}
