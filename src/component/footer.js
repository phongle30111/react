import React from "react";
import "../component/css/footer.css";

const Footer = () => {
  return (
    <footer>
      <div className="container">
        <div className="row">
          <div className="col-6 d-flex flex-column align-items-start">
            <h4>Thông tin liên hệ</h4>
            <p>Email: support@danang.com</p>
            <p>Phone: 1900 1009</p>
            <p>Address: 123 Điện Biên Phủ, phường Chính Gián, quận Thanh Khê, thành phố Đà Nẵng</p>
          </div>
          <div className="col-6 ">
            <h4>Theo dõi chúng tôi</h4>
            <a href="/" className="p-2">
              <i className="fa fa-facebook"></i>
            </a>

            <a href="/" className="p-2">
              <i className="fa fa-twitter"></i>
            </a>

            <a href="/" className="p-2">
              <i className="fa fa-instagram"></i>
            </a>

            <a href="/" className="p-2">
              <span className="fa fa-google-plus"></span>
            </a>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
