import React, { useState, useEffect } from "react";
import axios from "axios";
import { useParams } from "react-router-dom";

export default function MedicineDetail() {
  // Lấy URL hiện tại của trang
  const url = useParams();
  const [medicine, setMedicine] = useState([]);

  useEffect(() => {
    axios
      .get(`http://localhost:8080/product/detail/${url.id}`)
      .then((response) => {
        console.log(response.data + "f");
        setMedicine(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [url.id]);

  return (
    <div className="container mt-5 mb-5">
      <div className="row justify-content-between">
        <div className="col-5 card h-100">
          <img src={medicine.img} alt="Medicine" />
        </div>
        <div className="col-6">
          <div className="row">
            <h4 className="card-title">{medicine.nameMedicine}</h4>
          </div>
          <div className="row">
            <span className="">Giá:</span>
            <h3 className="text-info font-weight-bold ml-2">
              {medicine.price}đ/{medicine.unit}
            </h3>
          </div>
          <div className="row">
            <p>Công dụng: {medicine.uses}</p>
          </div>
          <div className="row">
            <p>Thành Phần: {medicine.mainIngredients}</p>
          </div>

          <div className="row">
            <button
              className="btn btn-primary"
              onClick={() => this.props.handleAddToCart(medicine)}
            >
              Thêm vào giỏ hàng
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
