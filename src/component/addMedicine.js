import React, { useState, useEffect } from "react";
import "../component/css/addMedicine.css";
import axios from "axios";
import { useNavigate } from "react-router-dom";

export default function AddMedicine() {
  const [medicine, setMedicine] = useState({
    medicineName: "",
    mainIngredients: "",
    uses: "",
    price: "",
    unit: "",
    quantity: "",
    category: "",
    img: null,
  });

  const [listCategory, setListCategory] = useState([]);
  // const [imgFile, setImgFile] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    axios.get("http://localhost:8080/category/list").then((response) => {
      setListCategory(response.data);
      console.log(response.data);
    });
  }, []);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setMedicine((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleAddMedicine = (e) => {
    e.preventDefault();
    const boundary = `----WebKitFormBoundary${Math.random()
      .toString(36)
      .substring(2)}`;
    // const formData = new FormData();
    // formData.append("medicine", medicine);
    // formData.append("img", imgFile);
    console.log(medicine.img);
    axios
      .post("http://localhost:8080/product/save", medicine, {
        headers: {
          "Content-Type": `multipart/form-data; boundary=${boundary}`,
        },
      })
      .then((response) => {
        navigate("/");
        console.log("addSuccess", response.data);
      })
      .catch((error) => {
        console.log("error:" + error);
        window.alert("thất bại");
      });
  };

  const handleCategoryChange = (e) => {
    const selectedCategoryId = e.target.value;
    const selectedCategory = listCategory.find(
      (category) => category.categoryId === selectedCategoryId
    );

    setMedicine((prevMedicine) => ({
      ...prevMedicine,
      category: {
        categoryId: selectedCategoryId,
        categoryName: selectedCategory,
      },
    }));
  };
  const handleFileChange = (e) => {
    const file = e.target.files[0];
    setMedicine((prevState) => ({
      ...prevState,
      img: file,
    }));
  };
  return (
    <div className="mb-4 d-flex justify-content-center">
      <form id="form-info" onSubmit={handleAddMedicine}>
        <h3>Thêm Sản Phẩm Thuốc</h3>

        <div>
          <label htmlFor="medicineName">Tên thuốc:</label>
          <input
            type="text"
            className="input-form"
            name="medicineName"
            onChange={handleChange}
          />
        </div>
        <div>
          <label htmlFor="mainIngredients">Thành phần chính:</label>
          <input
            type="text"
            className="input-form"
            name="mainIngredients"
            onChange={handleChange}
          />
        </div>
        <div>
          <label htmlFor="uses">Công dụng:</label>
          <textarea
            className="input-form"
            name="uses"
            onChange={handleChange}
            rows={5}
          ></textarea>
        </div>
        <div>
          <label htmlFor="price">Giá:</label>
          <input
            type="number"
            className="input-form"
            name="price"
            onChange={handleChange}
          />
        </div>
        <div>
          <label htmlFor="unit">Đơn vị:</label>
          <input
            type="text"
            className="input-form"
            name="unit"
            onChange={handleChange}
          />
        </div>
        <div>
          <label htmlFor="quantity">Số lượng:</label>
          <input
            type="number"
            className="input-form"
            name="quantity"
            onChange={handleChange}
          />
        </div>
        <div>
          <label htmlFor="img">Hình ảnh:</label>
          <input
            type="file"
            className="input-form"
            name="img"
            onChange={handleFileChange}
          />
        </div>
        <div>
          <label htmlFor="categoryId">Danh mục:</label>
          <select
            className="input-form"
            name="categoryId"
            onChange={handleCategoryChange}
          >
            <option>Chọn loại thuốc...</option>
            {listCategory.map((item) => (
              <option key={item.categoryId} value={item.categoryId}>
                {item.categoryName}
              </option>
            ))}
          </select>
        </div>
        <div className="mt-5 d-flex justify-content-center">
          <button type="Submit">Thêm mới</button>
        </div>
      </form>
    </div>
  );
}
