import React, { useEffect, useState } from "react";
import axios from "axios";
import "../component/css/form.module.css";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";

export default function Login({ setIsLoggedIn }) {
  const [user, setUser] = useState({
    userAccount: "",
    password: "",
  });
  const navigate = useNavigate();

  useEffect(() => {
    const checkLogin = localStorage.getItem("user");
    if (checkLogin != null) {
      navigate("/");
    }
  }, [navigate]);

  const handleLogin = (e) => {
    e.preventDefault();
    axios
      .post("http://localhost:8080/user/login", user)
      .then((response) => {
        localStorage.setItem("user", response);
        console.log(response);
        navigate("/");
        setIsLoggedIn(true);
      })
      .catch((error) => {
        navigate("/login");
      });
  };

  const handleChange = (event) => {
    const { name, value } = event.target;
    setUser((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  return (
    <div className="mb-4 d-flex justify-content-center">
      <form onSubmit={handleLogin}>
        <h3>Login</h3>

        <label htmlFor="userAccount">Username</label>
        <input
          type="text"
          name="userAccount"
          onChange={handleChange}
          placeholder="Username"
        />
        <label htmlFor="password">Password</label>
        <input
          type="password"
          name="password"
          onChange={handleChange}
          placeholder="Password"
        />
        <Link to="/register">
          <h6 class="mt-4">Register?</h6>
        </Link>
        <div className="mt-4 d-flex justify-content-center">
          <button>Log In</button>
        </div>
      </form>
    </div>
  );
}
