import React from "react";
import "../component/css/header.css";

const Header = () => {
  return (
    <div className="top-bar p-2 bg-dark">
      <div className="container">
        <div className="row">
          <div className="col-md-6">
            <p className="mb-0">
              <span className="mr-3">
                <strong className="text-white">Phone:</strong>
                <a className="a-color" href="tel:///">
                  {" "}
                  1900 1009
                </a>
              </span>
              <span>
                <strong className="text-white">Email:</strong>
                <a className="a-color" href="/">
                  {" "}
                  support@danang.com
                </a>
              </span>
            </p>
          </div>
          <div className="col-md-6">
            <ul className="social-media">
              <li>
                <a href="/" className="p-2">
                  <i className="fa fa-facebook"></i>
                </a>
              </li>
              <li>
                <a href="/" className="p-2">
                  <i className="fa fa-twitter"></i>
                </a>
              </li>
              <li>
                <a href="/" className="p-2">
                  <i className="fa fa-instagram"></i>
                </a>
              </li>
              <li>
                <a href="/" className="p-2">
                  <span className="fa fa-google-plus"></span>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Header;
