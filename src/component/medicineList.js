import React, { useState, useEffect } from "react";
import MedicineCard from "./medicineCard";
import axios from "axios";
import { Link } from "react-router-dom";
import "../component/css/medicineList.module.css";

export default function MedicineList(props) {
  const { handleAddToCart } = props;
  const [listMedicine, setListMedicine] = useState([]);
  const [activeCategory, setActiveCategory] = useState("all");

  useEffect(() => {
    axios.get("http://localhost:8080/product/list").then((response) => {
      setListMedicine(response.data);
    });
  }, []);

  const handleCategoryChange = (category) => {
    setActiveCategory(category);
  };

  const filteredMedicines =
    activeCategory === "all"
      ? listMedicine
      : listMedicine.filter((medicine) => medicine.category === activeCategory);

  return (
    <div>
      <section className="product-section">
        <div className="container">
        <div className="row">
              <Link to="add-medicine">
                <h1 className=" site-logo">addMedicine</h1>
              </Link>
            </div>
          <div className="row d-flex justify-content-around mb-4">
            <button
              className={activeCategory === "all" ? "active" : ""}
              onClick={() => handleCategoryChange("all")}
            >
              Tất cả
            </button>
            <button
              className={activeCategory === "1" ? "active" : ""}
              onClick={() => handleCategoryChange("1")}
            >
              Giảm đau
            </button>
            <button
              className={activeCategory === "2" ? "active" : ""}
              onClick={() => handleCategoryChange("2")}
            >
              Vitamin
            </button>
            <button
              className={activeCategory === "3" ? "active" : ""}
              onClick={() => handleCategoryChange("3")}
            >
              Tiêu hóa
            </button>
            <button
              className={activeCategory === "4" ? "active" : ""}
              onClick={() => handleCategoryChange("4")}
            >
              Xương khớp
            </button>
          </div>
          <div className="row">
            {filteredMedicines.map((item) => (
              <MedicineCard medicine={item} handleAddToCart={handleAddToCart} />
            ))}
          </div>
        </div>
      </section>
    </div>
  );
}
