import React from "react";
import "../component/css/nav.css";
import { Link } from "react-router-dom";

export default function Nav({ cartItems, isLoggedIn, setIsLoggedIn }) {
  const countMedicine = cartItems.length;

  const handleLogout = () => {
    localStorage.clear();
    setIsLoggedIn(false);
  };
  return (
    <nav>
      <div className="pt-3 pb-3 mb-4 box-shadow-bottom">
        <div className="container">
          <div className="row d-flex flex-row align-items-between">
            <div className="col-2">
              <Link to="">
                <h1 className=" site-logo">Medicine</h1>
              </Link>
            </div>
            
            <div className="col-10 d-flex justify-content-end align-items-center">
              <div className="user-icon-container mr-3">
                <div className="cart-icon">
                  <i className="fa fa-user"></i>
                  <div className="user-info"></div>
                </div>
                {isLoggedIn ? (
                  <div>
                    <Link to="/">
                      <button className="infomation-button">
                        Hồ sơ của tôi
                      </button>
                    </Link>
                    <Link to="/">
                      <button className="logout-button" onClick={handleLogout}>
                        Đăng xuất
                      </button>
                    </Link>
                  </div>
                ) : (
                  <Link to="/login">
                    <button className="login-button">Đăng nhập</button>
                  </Link>
                )}
              </div>
              <Link to="/cart" className="text-white">
                <div className="cart-icon">
                  {countMedicine > 0 && (
                    <span className="cart-count">{countMedicine}</span>
                  )}
                  <i className="fa fa-shopping-cart"></i>
                </div>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </nav>
  );
}
