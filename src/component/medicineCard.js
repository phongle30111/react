import React from "react";
import { Link } from "react-router-dom";

export default function MedicineCard(props) {
  const { medicine, handleAddToCart } = props;

  return (
    <div className="col-3 mb-4">
      <div className="card h-100">
        <Link to={`/medicine/detail/${medicine.medicineId}`}>
          <img src={medicine.img} className="card-img-top" alt="Product" />
        </Link>
        <div className="card-body d-flex flex-column justify-content-between">
          <h5 className="card-title">{medicine.medicineName}</h5>
          <div className="d-flex flex-column">
            <p className="card-text"> 
              {medicine.price.toLocaleString()}đ/{medicine.unit}
            </p>
            <button
              className="btn btn-primary"
              onClick={() => handleAddToCart(medicine)}
            >
              Thêm vào giỏ hàng
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
